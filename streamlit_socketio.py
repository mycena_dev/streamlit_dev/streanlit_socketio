import streamlit as st
import socketio
import asyncio

data_buffer = []

async def socketio_handler():
    global logger
    sio = socketio.Client(logger=False, engineio_logger=False)

    @sio.event
    def text(data):
        # print(f'text message received with {data}')
        data_buffer.append(data)

    sio.connect('http://localhost:5000')
    await asyncio.sleep(1)

async def render_num_thread():
    while True:
        if data_buffer:
            data = data_buffer.pop(0)
            if data:
                st.write(data)

asyncio.run(socketio_handler())
asyncio.run(render_num_thread())

