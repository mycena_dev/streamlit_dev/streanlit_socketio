import time
import socketio
import logging
import threading
import random


class CustomFormatter(logging.Formatter):

    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    # format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s (%(filename)s:%(lineno)d)"

    FORMATS = {
        logging.DEBUG: grey + format + reset,
        logging.INFO: grey + format + reset,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: bold_red + format + reset
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)

logger = logging.getLogger("socketio_server")
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(CustomFormatter())
logger.addHandler(ch)



sio = socketio.Server(cors_allowed_origins='*', async_mode='gevent')
app = socketio.WSGIApp(sio, static_files={
    '/': {'content_type': 'text/html', 'filename': 'index.html'}
})


@sio.event
def connect(sid, environ):
    logger.warning(f'connect {sid}' )

@sio.event
def my_message(sid, data):
    logger.warning(f'message {data}')

@sio.event
def disconnect(sid):
    logger.warning(f'disconnect {sid}')

def send_text():
    global sio
    while True:
        try:
            data = str(random.randbytes(10))
            sio.emit("text", data)
            logger.warning(f'send text {data}')
            time.sleep(1)
        except Exception as e:
            print(e)

if __name__ == '__main__':
    t = threading.Thread(target=send_text, daemon=True)
    t.start()
    from gevent import pywsgi
    pywsgi.WSGIServer(('', 5000), app).serve_forever()
