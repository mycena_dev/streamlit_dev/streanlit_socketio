# Requirement

python: 3.10.6

eventlet: 0.33.2

gevent: 22.10.2

gevent_socketio: 0.3.6

streamlit: 1.13.0

python-socketio: 5.7.2

---

# How to run

## Step1. create python 3.10.6 env.

```
conda create --name python3.10.6 python=3.10.6
```

## Step2. Switch to python 3.10.6 env

```
activate python3.10.6
```

## Step3. Install requirements.

```
pip install -r requirements.txt
```

## Step4. Run the socketio server.

```
python socketio_server.py
```

## Step5. Run the streamlit.
```
streamlit run streamlit_socketio.py
```